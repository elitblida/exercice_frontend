### Exercice
******
- This is an application that gives you the ability to manage (CRUD) books with book families ,  this application is fully secured with jwt authentication from spring security


### Installation dependencies
******

- NPM
- angular 14


### Some dependencies chosen

- primeng :  UI components for Angular
- primeFlex :  is a lightweight responsive CSS
- file-saver  : manage excel exportation


#### Docker
******
to run this application as container in docker u should
- go to frontend project in the terminal
- create your jar file `ng build`
- exec
- `docker build -t exercise-front .`
- `docker run -p 80:80 exercise-front`

Demo : go to    127.0.0.1

#### Backend Application
******
- go visit : https://gitlab.com/elitblida/exercise_backend.git


import {Family} from "./family";

export class Book {
  id?: string;
  name?: string;
  author?: string;
  copyright?: string;
  description?: number;
  imagePath?: string;
  families?: Family[];
}

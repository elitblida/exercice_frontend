import { Injectable } from '@angular/core';

const TOKEN_TYPE = 'jwtTokenType';
const TOKEN = 'jwtToken';

@Injectable({
  providedIn: 'root'
})
export class LocalstorageService {

  setToken(tokenType: string , accessToken: string ) {
    localStorage.setItem(TOKEN_TYPE, tokenType);
    localStorage.setItem(TOKEN, accessToken);
  }

  getToken(): string {
    return <string>localStorage.getItem(TOKEN);
  }

  getTokenType(): string {
    return <string>localStorage.getItem(TOKEN_TYPE);
  }

  removeToken() {
    localStorage.removeItem(TOKEN_TYPE);
    localStorage.removeItem(TOKEN);
  }
}

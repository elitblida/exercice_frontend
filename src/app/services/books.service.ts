import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Book } from '../models/book';
import {environment} from "../../environments/environment";


@Injectable({
  providedIn: 'root'
})
export class BooksService {
  apiURLBooks = environment.apiUrl + 'books';

  constructor(private http: HttpClient) {}

  createBook(BookData: FormData): Observable<Book> {
    return this.http.post<Book>(this.apiURLBooks, BookData);
  }

  getBook(BookId: string): Observable<Book> {
    return this.http.get<Book>(`${this.apiURLBooks}/${BookId}`);
  }


  getBooksAsExcel(): Observable<any> {
    // @ts-ignore
    return this.http.get<any>(`${this.apiURLBooks}/export/excel`, {responseType: 'blob'});
  }


  getBooks(): Observable<Book[]> {
    return this.http.get<Book[]>(this.apiURLBooks);
  }

  updateBook(BookData: FormData, BookId: string): Observable<Book> {
    return this.http.put<Book>(`${this.apiURLBooks}/${BookId}`, BookData);
  }

  deleteBook(BookId: string): Observable<any> {
    return this.http.delete<any>(`${this.apiURLBooks}/${BookId}`);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from "../../environments/environment";
import {Family} from "../models/family";


@Injectable({
  providedIn: 'root'
})
export class FamiliesService {
  apiURLFamilies = environment.apiUrl + 'families';

  constructor(private http: HttpClient) {}

   options = {
    responseType: 'blob'
  };
  createFamily(FamilyData: FormData): Observable<Family> {
    return this.http.post<Family>(this.apiURLFamilies, FamilyData);
  }

  getFamily(FamilyId: string): Observable<Family> {
    return this.http.get<Family>(`${this.apiURLFamilies}/${FamilyId}`);
  }

  getFamiliesAsExcel(): Observable<any> {
    // @ts-ignore
    return this.http.get<any>(`${this.apiURLFamilies}/export/excel`, {responseType: 'blob'});
  }

  getFamilies(): Observable<Family[]> {
    return this.http.get<Family[]>(this.apiURLFamilies);
  }

  updateFamily(FamilyData: FormData, FamilyId: string): Observable<Family> {
    return this.http.put<Family>(`${this.apiURLFamilies}/${FamilyId}`, FamilyData);
  }

  deleteFamily(FamilyId: string): Observable<any> {
    return this.http.delete<any>(`${this.apiURLFamilies}/${FamilyId}`);
  }
}

import {HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LocalstorageService } from './localstorage.service';
import {environment} from "../../environments/environment";
import {Auth} from "../models/auth";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  apiURLAuth = environment.apiUrl + 'auth/signin';

  constructor(
    private http: HttpClient,
    private token: LocalstorageService,
    private router: Router
  ) {}

  login(username: string, password: string): Observable<Auth> {
    return this.http.post<Auth>(this.apiURLAuth, { username, password });
  }

  logout() {
    this.token.removeToken();
    this.router.navigate(['/login']);
  }
}

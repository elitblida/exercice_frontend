import {Injectable} from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse} from '@angular/common/http';
import {catchError, Observable} from 'rxjs';
import {LocalstorageService} from "../services/localstorage.service";
import {environment} from "../../environments/environment";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private localstorageToken: LocalstorageService) {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const token = this.localstorageToken.getToken();
    const tokenType = this.localstorageToken.getTokenType();
    const isAPIUrl = request.url.startsWith(environment.apiUrl);

    if (token && isAPIUrl) {
      request = request.clone({
        setHeaders: {
          Authorization: tokenType + ' ' + token
        }
      });
    }

    // @ts-ignore
    return next.handle(request).pipe(
      catchError((err) => {
          console.log(err)
          if (err.status === 401) {
            this.localstorageToken.removeToken();
          }
          return err
        }
      )
    );
  }
}

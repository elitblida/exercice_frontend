
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {AuthService} from "../services/auth.service";
import {LocalstorageService} from "../services/localstorage.service";
import {Auth} from "../models/auth";

@Component({
  selector: 'users-login',
  templateUrl: './login.component.html',
  styles: []
})
export class LoginComponent implements OnInit {
  loginFormGroup!: FormGroup;
  isSubmitted = false;
  authError = false;
  authMessage: string | undefined = 'Email or Password are wrong';

  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private localstorageService: LocalstorageService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this._initLoginForm();
  }

  private _initLoginForm() {
    this.loginFormGroup = this.formBuilder.group({
      username: ['othmane', Validators.required],
      password: ['1', Validators.required]
    });
  }

  onSubmit() {
    this.isSubmitted = true;

    if (this.loginFormGroup.invalid) return;

    this.auth.login(this.loginForm['username'].value, this.loginForm['password'].value).subscribe(
      (auth :Auth) => {
          this.authError = false;
          this.localstorageService.setToken(auth.tokenType +'',auth.accessToken+'' );
          this.router.navigate(['/']);
      },
      () => {
        this.authError = true;
      }
    );
  }

  get loginForm() {
    return this.loginFormGroup.controls;
  }
}

import {Route} from '@angular/router';
import {BookComponent} from "./book/book.component";
import {BookAddComponent} from "./book/book-add/book-add.component";
import {BookEditComponent} from "./book/book-edit/book-edit.component";
import {LoginComponent} from "./login/login.component";
import {LayoutComponent} from "./layout/layout.component";
import {AuthGuard} from "./guards/auth-guard.service";
import {FamilyAddComponent} from "./family/family-add/family-add.component";
import {FamilyComponent} from "./family/family.component";
import {FamilyEditComponent} from "./family/family-edit/family-edit.component";


export const appRoutes: Route[] = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: '',
    component: LayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'books',
        component: BookComponent,
      },
      {
        path: 'books/add',
        component: BookAddComponent,
      },
      {
        path: 'books/edit/:id',
        component: BookEditComponent,
      },
      {
        path: 'families',
        component: FamilyComponent,
      },
      {
        path: 'families/add',
        component: FamilyAddComponent,
      },
      {
        path: 'families/edit/:id',
        component: FamilyEditComponent,
      },
      {path: '', redirectTo: '/books', pathMatch: 'full'},
    ]
  },
  {path: '**', redirectTo: '/books', pathMatch: 'full'},
];

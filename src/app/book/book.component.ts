import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import {BooksService} from "../services/books.service";
import {Book} from "../models/book";
// @ts-ignore
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styles: []
})
export class BookComponent implements OnInit, OnDestroy {

  books : Book[] =[] ;
  endsubs$: Subject<any> = new Subject();

  constructor(
    private booksService: BooksService,
    private router: Router,
    private messageService: MessageService,
    private confirmationService: ConfirmationService
  ) {}

  ngOnInit(): void {
    this._getBooks();
  }


  ngOnDestroy() {
    this.endsubs$.next('');
    this.endsubs$.complete();
  }

  private _getBooks() {
    this.booksService
      .getBooks()
      .pipe(takeUntil(this.endsubs$))
      .subscribe((books) => {
        this.books = books;
      });
  }

  updateBook(BookId: string) {
    this.router.navigateByUrl(`books/edit/${BookId}`);
  }

  deleteBook(bookId: string) {
    this.confirmationService.confirm({
      message: 'Do you want to delete this Book?',
      header: 'Delete Book',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.booksService
          .deleteBook(bookId)
          .pipe(takeUntil(this.endsubs$))
          .subscribe(
            () => {
              this._getBooks();
              this.messageService.add({
                severity: 'success',
                summary: 'Success',
                detail: 'Book is deleted!'
              });
            },
            () => {
              this.messageService.add({
                severity: 'error',
                summary: 'Error',
                detail: 'Book is not deleted!'
              });
            }
          );
      }
    });
  }
  exportFamilies(){
    this.booksService
      .getBooksAsExcel()
      .pipe(takeUntil(this.endsubs$))
      .subscribe(data => saveAs(data, `Books.xlsx`));
  }
}

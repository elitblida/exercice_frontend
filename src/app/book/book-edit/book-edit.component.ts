import {Location} from '@angular/common';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {MessageService} from 'primeng/api';
import {Subject, timer} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {BooksService} from "../../services/books.service";
import {Book} from "../../models/book";
import {Family} from "../../models/family";
import {FamiliesService} from "../../services/family.service";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'book-edit',
  templateUrl: './book-edit.component.html',
  styles: []
})
export class BookEditComponent implements OnInit, OnDestroy {
  form!: FormGroup;
  isSubmitted = false;
  imageDisplay!: string | ArrayBuffer | undefined;
  bookId !:string ;
  FamiliesList : Family[] =[];
  bookImagesUrl = environment.bookImagesUrl;
  selectedFamilies!: (string | undefined)[] | undefined ;
  endsubs$: Subject<any> = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    private booksService: BooksService,
    private FamiliesService: FamiliesService,
    private messageService: MessageService,
    private location: Location,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this._initForm();
    this._getFamilies();
    this._initFormValues();
  }

  ngOnDestroy() {
    this.endsubs$.next('');
    this.endsubs$.complete();
  }

  private _initForm() {
    this.form = this.formBuilder.group({
      name: ['', [ Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50)]],
      author: ['',[ Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50)]],
      copyright: ['',[ Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50)]],
      description: ['',[ Validators.maxLength(250)]],
      image: ['', ],
    });
  }
  private _getFamilies() {
    this.FamiliesService.getFamilies().subscribe((Families) => {
      this.FamiliesList = Families;
    });
  }

  private _updateBook(bookFormData: FormData) {
    this.booksService
      .updateBook(bookFormData, this.bookId)
      .pipe(takeUntil(this.endsubs$))
      .subscribe(
        () => {
          this.messageService.add({
            severity: 'success',
            summary: 'Success',
            detail: 'Book is updated!'
          });
          timer(1000)
            .toPromise()
            .then(() => {
              this.location.back();
            });
        },
        () => {
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: 'Book is not updated!'
          });
        }
      );
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.form.invalid) return;
    const bookFormData = new FormData();
    Object.keys(this.bookForm).map((key) => {
      bookFormData.append(key, this.bookForm[key].value);
    });
    bookFormData.append('selectedFamilies', JSON.stringify(this.selectedFamilies).replace(/]|[[]/g, ''));
    this._updateBook(bookFormData);

  }

  onCancle() {
    this.location.back();
  }

  // @ts-ignore
  onImageUpload(event) {
    const file = event.target.files[0];
    if (file) {
      this.form.patchValue({ image: file });
      // @ts-ignore
      this.form.get('image').updateValueAndValidity();
      const fileReader = new FileReader();
      fileReader.onload = () => {
        // @ts-ignore
        this.imageDisplay = fileReader.result;
      };
      fileReader.readAsDataURL(file);
    }
  }

  get bookForm() {
    return this.form.controls;
  }

  private _initFormValues() {
    this.route.params.pipe(takeUntil(this.endsubs$)).subscribe((params) => {
      this.bookId = params['id'];
    });
    this.booksService
      .getBook(this.bookId)
      .pipe(takeUntil(this.endsubs$))
      .subscribe((book) => this._updateFormBook(book));
  }


  private _updateFormBook(book:Book){
    this.bookForm['name'].setValue(book.name);
    this.bookForm['author'].setValue(book.author);
    this.bookForm['copyright'].setValue(book.copyright);
    this.bookForm['description'].setValue(book.description);
    this.imageDisplay = this.bookImagesUrl + book.imagePath;
    this.bookForm['image'].setValidators([]);
    this.bookForm['image'].updateValueAndValidity();
    this.selectedFamilies = book.families?.map(value => value.id);
  }
}

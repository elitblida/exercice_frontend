import {Location} from '@angular/common';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MessageService} from 'primeng/api';
import {Subject, timer} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {BooksService} from "../../services/books.service";
import {Book} from "../../models/book";
import {FamiliesService} from "../../services/family.service";
import {Family} from "../../models/family";

@Component({
  selector: 'book-add',
  templateUrl: './book-add.component.html',
  styles: []
})
export class BookAddComponent implements OnInit, OnDestroy {
  form!: FormGroup;
  isSubmitted = false;
  imageDisplay!: string | ArrayBuffer;
  FamiliesList : Family[] =[];
  selectedFamilies: number[] =[];
  endsubs$: Subject<any> = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    private booksService: BooksService,
    private familiesService: FamiliesService,
    private messageService: MessageService,
    private location: Location
  ) {
  }

  ngOnInit(): void {
    this._initForm();
    this._getFamilies();
  }

  ngOnDestroy() {
    this.endsubs$.next('');
    this.endsubs$.complete();
  }

  private _initForm() {
    this.form = this.formBuilder.group({
      name: ['book name',  [ Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50)]],
      author: ['author',  [ Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50)]],
      copyright: ['copyright',  [ Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50)]],
      description: ['',[ Validators.maxLength(250)]],
      image: ['',],
    });
  }

  private _getFamilies() {
    this.familiesService.getFamilies().subscribe((Families) => {
      this.FamiliesList = Families;
    });
  }
  private _addBook(bookData: FormData) {
    this.booksService
      .createBook(bookData)
      .pipe(takeUntil(this.endsubs$))
      .subscribe(
        (book: Book) => {
          this.messageService.add({
            severity: 'success',
            summary: 'Success',
            detail: `Book ${book.name} is created!`
          });
          timer(1000)
            .toPromise()
            .then(() => {
              this.location.back();
            });
        },
        () => {
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: 'Book is not created!'
          });
        }
      );
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.form.invalid) return;
    const bookFormData = new FormData();
    Object.keys(this.bookForm).map((key) => {
      bookFormData.append(key, this.bookForm[key].value);
    });
    bookFormData.append('selectedFamilies', JSON.stringify(this.selectedFamilies).replace(/]|[[]/g, ''));
    this._addBook(bookFormData);

  }

  onCancle() {
    this.location.back();
  }

  // @ts-ignore
  onImageUpload(event) {
    const file = event.target.files[0];
    if (file) {
      this.form.patchValue({ image: file });
      // @ts-ignore
      this.form.get('image').updateValueAndValidity();
      const fileReader = new FileReader();
      fileReader.onload = () => {
        // @ts-ignore
        this.imageDisplay = fileReader.result;
      };
      fileReader.readAsDataURL(file);
    }
  }

  get bookForm() {
    return this.form.controls;
  }

}

import {Directive, ElementRef, HostListener, Renderer2} from "@angular/core";
import {environment} from "../../environments/environment";

@Directive({
  selector: "img[appHideMissing]",
})
export class HideMissingDirective {

  defaultImageDisplay = environment.bookImagesUrl + 'default.jpg';

  constructor(private el: ElementRef, private renderer: Renderer2) {
  }

  @HostListener("error")
  private onError() {
      this.renderer.setAttribute(this.el.nativeElement, 'src', this.defaultImageDisplay);
  }
}

import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from "@angular/router";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {appRoutes} from "./app.routing";

//----------------- COMPONENTS
import {AppComponent} from './app.component';
import {SidebarComponent} from "./layout/sidebar/sidebar.component";
import {LayoutComponent} from "./layout/layout.component";
import {BookComponent} from "./book/book.component";
import {BookAddComponent} from "./book/book-add/book-add.component";
import {BookEditComponent} from "./book/book-edit/book-edit.component";
import {MultiSelectModule} from "primeng/multiselect";
import {LoginComponent} from "./login/login.component";
import {ConfirmationService} from "primeng/api";
import {FamilyComponent} from "./family/family.component";
import {FamilyAddComponent} from "./family/family-add/family-add.component";
import {FamilyEditComponent} from "./family/family-edit/family-edit.component";

//----------------- PRIMING
import {ToastModule} from "primeng/toast";
import {ToolbarModule} from "primeng/toolbar";
import {ButtonModule} from "primeng/button";
import {InputTextModule} from "primeng/inputtext";
import {InputTextareaModule} from "primeng/inputtextarea";
import {TableModule} from "primeng/table";
import {CardModule} from "primeng/card";
import {ConfirmDialogModule} from "primeng/confirmdialog";
import {ColorPickerModule} from "primeng/colorpicker";
import { JwtInterceptor } from './interceptors/jwt.interceptor';
import {HideMissingDirective} from "./directives/HideMissingDirective.directive";




const PRIMNG_MODULS = [
  CardModule,
  ToastModule,
  InputTextModule,
  TableModule,
  ButtonModule,
  ToolbarModule,
  ConfirmDialogModule,
  ColorPickerModule,
  InputTextareaModule,
  MultiSelectModule
];

const BOOK_COMPONENTS = [
  BookComponent,
  BookAddComponent,
  BookEditComponent,
];
const BOOK_FAMILY_COMPONENTS = [
  FamilyComponent,
  FamilyEditComponent,
  FamilyAddComponent
];

@NgModule({
  declarations: [
    AppComponent,
    HideMissingDirective,
    SidebarComponent,
    LayoutComponent,
    ...BOOK_COMPONENTS,
    ...BOOK_FAMILY_COMPONENTS,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    ...PRIMNG_MODULS,
    RouterModule.forRoot(appRoutes),
  ],
  providers: [
    ConfirmationService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule {
}

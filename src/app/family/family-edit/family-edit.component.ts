import {Location} from '@angular/common';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {MessageService} from 'primeng/api';
import {Subject, timer} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {FamiliesService} from "../../services/family.service";
import {Family} from "../../models/family";

@Component({
  selector: 'family-edit',
  templateUrl: './family-edit.component.html',
  styles: []
})
export class FamilyEditComponent implements OnInit, OnDestroy {
  form!: FormGroup;
  isSubmitted = false;
  familyId !:string ;
  endsubs$: Subject<any> = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    private familiesService: FamiliesService,
    private messageService: MessageService,
    private location: Location,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this._initForm();
    this._initFormValues();
  }

  ngOnDestroy() {
    this.endsubs$.next('');
    this.endsubs$.complete();
  }

  private _initForm() {
    this.form = this.formBuilder.group({
      name: ['', [ Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50)]],
      description: ['', [ Validators.maxLength(50)]]});
  }

  private _initFormValues() {
    this.route.params.pipe(takeUntil(this.endsubs$)).subscribe((params) => {
      this.familyId = params['id'];
    });
    this.familiesService
      .getFamily(this.familyId)
      .pipe(takeUntil(this.endsubs$))
      .subscribe((family) => this._updateFormFamily(family));
  }


  private _updateFamily(familyFormData: FormData) {
    this.familiesService
      .updateFamily(familyFormData, this.familyId)
      .pipe(takeUntil(this.endsubs$))
      .subscribe(
        () => {
          this.messageService.add({
            severity: 'success',
            summary: 'Success',
            detail: 'Book Family is updated!'
          });
          timer(1000)
            .toPromise()
            .then(() => {
              this.location.back();
            });
        },
        () => {
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: 'Book Family is not updated!'
          });
        }
      );
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.form.invalid) return;
    const familyFormData = new FormData();
    Object.keys(this.familyForm).map((key) => {
      familyFormData.append(key, this.familyForm[key].value);
    });
    this._updateFamily(familyFormData);
  }

  onCancle() {
    this.location.back();
  }

  private _updateFormFamily(family:Family){
    this.familyForm['name'].setValue(family.name);
    this.familyForm['description'].setValue(family.description);
  }

  get familyForm() {
    return this.form.controls;
  }

}

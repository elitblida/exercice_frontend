import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import {Family} from "../models/family";
import {FamiliesService} from "../services/family.service";
// @ts-ignore
import { saveAs } from 'file-saver';
@Component({
  selector: 'app-family',
  templateUrl: './family.component.html',
  styles: []
})
export class FamilyComponent implements OnInit, OnDestroy {

  families : Family[] =[] ;
  endsubs$: Subject<any> = new Subject();
  constructor(
    private familiesService: FamiliesService,
    private router: Router,
    private messageService: MessageService,
    private confirmationService: ConfirmationService
  ) {}

  ngOnInit(): void {
    this._getFamilies();
  }

  ngOnDestroy() {
    this.endsubs$.next('');
    this.endsubs$.complete();
  }

  private _getFamilies() {
    this.familiesService
      .getFamilies()
      .pipe(takeUntil(this.endsubs$))
      .subscribe((families) => {
        this.families = families;
      });
  }

  updateFamily(BookId: string) {
    this.router.navigateByUrl(`families/edit/${BookId}`);
  }

  deleteFamily(familyId: string) {
    this.confirmationService.confirm({
      message: 'Do you want to delete this Book Family?',
      header: 'Delete Book Family',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.familiesService
          .deleteFamily(familyId)
          .pipe(takeUntil(this.endsubs$))
          .subscribe(
            () => {
              this._getFamilies();
              this.messageService.add({
                severity: 'success',
                summary: 'Success',
                detail: 'Book is deleted!'
              });
            },
            () => {
              this.messageService.add({
                severity: 'error',
                summary: 'Error',
                detail: 'Book is not deleted!'
              });
            }
          );
      }
    });
  }

  exportFamilies(){
    this.familiesService
      .getFamiliesAsExcel()
      .pipe(takeUntil(this.endsubs$))
      .subscribe(data => saveAs(data, `families.xlsx`));
  }
}

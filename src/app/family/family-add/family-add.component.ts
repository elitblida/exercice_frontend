import {Location} from '@angular/common';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl, FormArray} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {MessageService} from 'primeng/api';
import {Subject, timer} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {FamiliesService} from "../../services/family.service";
import {Family} from "../../models/family";


@Component({
  selector: 'family-add',
  templateUrl: './family-add.component.html',
  styles: []
})
export class FamilyAddComponent implements OnInit, OnDestroy {
  form!: FormGroup;
  isSubmitted = false;
  endsubs$: Subject<any> = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    private familiesService: FamiliesService,
    private messageService: MessageService,
    private location: Location
  ) {
  }

  ngOnInit(): void {
    this._initForm();
  }

  ngOnDestroy() {
    this.endsubs$.next('');
    this.endsubs$.complete();
  }

  private _initForm() {
    this.form = this.formBuilder.group({
      name: ['book family name', [ Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50)]],
      description: ['description', [ Validators.maxLength(50)]]});
  }

  private _addFamily(familyData: FormData) {
    this.familiesService
      .createFamily(familyData)
      .pipe(takeUntil(this.endsubs$))
      .subscribe(
        (familyData: Family) => {
          this.messageService.add({
            severity: 'success',
            summary: 'Success',
            detail: `Family ${familyData.name} is created!`
          });
          timer(1000)
            .toPromise()
            .then(() => {
              this.location.back();
            });
        },
        () => {
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: 'Family is not created!'
          });
        }
      );
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.form.invalid) return;
    const familyFormData = new FormData();
    Object.keys(this.familyForm).map((key) => {
      familyFormData.append(key, this.familyForm[key].value);
    });
    this._addFamily(familyFormData);
  }

  onCancle() {
    this.location.back();
  }

  get familyForm() {
    return this.form.controls;
  }

}

export const environment = {
    production: false,
    apiUrl: 'http://localhost:8088/',
    bookImagesUrl:'./assets/images/local_storage/'
};

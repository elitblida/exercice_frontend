export const environment = {
  production: true,
  apiUrl: 'http://localhost:8088/',
  bookImagesUrl:'./assets/images/local_storage/'
};
